using System;

public static class ArrayPrinter
{
	public static void Print(IntArray arr)
	{
		for (int i = 0; i < arr.Size; i++)
		{
			Console.Write("{0}\t", arr[i]);
		}
		Console.WriteLine("{0}{0}", Environment.NewLine);
	}
}

public class IntArray
{	
	// Private fields
	private int[] _intArray;
	private int _size;
	
	// Constructors
	public IntArray(int size)
	{
		if (size < 0)
		{
			throw new ArgumentException("Cannot create an array with a negative length.");
		}
		
		_size = size;
		_intArray = new int[size];
	}
	
	// Properties
	public int Size 
	{
		get { return _size; }
	}
	
	// Indexers
	public int this[int index]
	{
		get
		{
			if (index < 0 || index >= _intArray.Length)
			{
				throw new IndexOutOfRangeException("Index out of range.");
			}
			
			return _intArray[index];
		}
		set
		{
			if (index < 0 || index >= _intArray.Length)
			{
				throw new IndexOutOfRangeException("Index out of range.");
			}
			
			_intArray[index] = value;
		}
	}
		
	// Operators
	public static IntArray operator + (IntArray arr1, IntArray arr2)
	{
		if (arr1._size != arr2._size)
		{
			throw new ArgumentException("Size of arrays not match.");
		}
		
		IntArray resultArray = new IntArray(arr1._size);
		
		for (int i = 0; i < arr1._size; i++)
		{
			resultArray[i] = arr1[i] + arr2[i];
		}
		
		return resultArray;
	}
	
	public static IntArray operator - (IntArray arr1, IntArray arr2)
	{
		if (arr1._size != arr2._size)
		{
			throw new ArgumentException("Size of arrays not match.");
		}
		
		IntArray resultArray = new IntArray(arr1._size);
		
		for (int i = 0; i < arr1._size; i++)
		{
			resultArray[i] = arr1[i] - arr2[i];
		}
		
		return resultArray;
	}
	
	public static IntArray operator * (IntArray arr, int value)
	{
		IntArray resultArray = new IntArray(arr._size);
		
		for (int i = 0; i < arr._size; i++)
		{
			resultArray[i] = arr[i] * value;
		}
		
		return resultArray;
	}
	
	public static IntArray operator / (IntArray arr, int value)
	{
		if (value == 0)
		{
			throw new ArgumentException("Divide by zero occurred");
		}
		
		IntArray resultArray = new IntArray(arr._size);
		
		for (int i = 0; i < arr._size; i++)
		{
			resultArray[i] = arr[i] / value;
		}
		
		return resultArray;
	}
	
	// Instance Methods
	public override bool Equals(object obj)
	{
		IntArray resultArray = obj as IntArray;
		
		if (resultArray._size != _size)
		{
			return false;
		}
		
		for (int i = 0; i < _size; i++)
		{
			if (resultArray[i] != this[i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	public override int GetHashCode()
	{
		throw new NotSupportedException();
	}
}

public class Program
{
	public static void Main()
	{
		Console.WriteLine("{0}***INTEGER ARRAYS PROGRAM***{0}", Environment.NewLine);

		try
        {
            // Create two arrays
            Console.WriteLine("---Create two integer arrays: firstArray and secondArray---");
            IntArray firstArray = new IntArray(8);
            IntArray secondArray = new IntArray(8);

            // Assign values of firstArray and secondArray
            Console.WriteLine("---Assign random values into firstArray and secondArray---");
            Random arrayRandom = new Random();
			
            for (int i = 0; i < firstArray.Size; i++)
            {
                firstArray[i] = arrayRandom.Next(-100, 100);
            }
			
            for (int i = 0; i < secondArray.Size; i++)
            {
                secondArray[i] = arrayRandom.Next(-200, 200);
            }

            // Print values of firstArray and secondArray
            Console.WriteLine("firstArray: ");
            ArrayPrinter.Print(firstArray);

            Console.WriteLine("secondArray: ");
            ArrayPrinter.Print(secondArray);

            // Add firstArray and secondArray
            Console.WriteLine("---Add firstArray and secondArray and result write in thirdArray---{0}", Environment.NewLine);
            IntArray thirdArray = firstArray + secondArray;

            // Print values of thirdArray
            Console.WriteLine("thirdArray: ");
            ArrayPrinter.Print(thirdArray);

            // Subtract firstArray and secondArray
            Console.WriteLine("---Subtract firstArray and secondArray and result write in fourthArray---{0}", Environment.NewLine);
            IntArray fourthArray = firstArray - secondArray;

            // Print values of fourthArray
            Console.WriteLine("fourthArray: ");
            ArrayPrinter.Print(fourthArray);

            // Multiple firstArray and integer
            Console.WriteLine("---Multiple firstArray and integer and result write in fifthArray---{0}", Environment.NewLine);
            int valueForMultiple = 85;
            IntArray fifthArray = firstArray * valueForMultiple;

            // Print values of fifthArray
            Console.WriteLine("Integer = {0}", valueForMultiple);
            Console.WriteLine("fifthArray: ");
            ArrayPrinter.Print(fifthArray);

            // Divide firstArray and integer
            Console.WriteLine("---Divide firstArray and integer and result write in sixthArray---{0}", Environment.NewLine);
            int valueForDivision = 10;
            IntArray sixthArray = firstArray / valueForDivision;

            // Print values of sixthArray
            Console.WriteLine("Integer = {0}", valueForDivision);
            Console.WriteLine("sixthArray: ");
            ArrayPrinter.Print(sixthArray);

            Console.WriteLine("---Compare firstArray and secondArray---{0}", Environment.NewLine);
            Console.WriteLine("Are the firstArray equal secondArray: {0}{1}", firstArray.Equals(secondArray), Environment.NewLine);

            Console.WriteLine("---Compare new seventhArray and new eightArray---{0}", Environment.NewLine);
            Console.WriteLine("---Create two integer arrays: seventhArray and eightArray---");
            IntArray seventhArray = new IntArray(5);
            IntArray eightArray = new IntArray(5);
			
            for (int i = 0; i < seventhArray.Size; i++)
            {
                seventhArray[i] = i;
            }
			
            for (int i = 0; i < eightArray.Size; i++)
            {
                eightArray[i] = i;
            }

            // Print values of seventhArray and eightArray
            Console.WriteLine("seventhArray: ");
            ArrayPrinter.Print(seventhArray);
            Console.WriteLine("eightArray: ");
            ArrayPrinter.Print(eightArray);
            Console.WriteLine("Are the seventhArray equal eightArray: {0}{1}", seventhArray.Equals(eightArray), Environment.NewLine);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
	}
}