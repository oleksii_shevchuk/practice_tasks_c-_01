using System;

public class Vector
{
	// Readonly fields
	public readonly double X;
	public readonly double Y;
	public readonly double Z;
	
	// Constructors
	public Vector(double x, double y, double z)
	{
		X = x;
		Y = y;
		Z = z;
	}
	
	// Operators
	public static Vector operator + (Vector v1, Vector v2)
	{
		return new Vector(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
	}
	
	public static Vector operator - (Vector v1, Vector v2)
	{
		return new Vector(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
	}
	
	// Static methods
	public static double GetScalarProduct(Vector v1, Vector v2)
	{
		return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
	}
	
	public static Vector GetProduct(Vector v1, Vector v2)
	{
		return new Vector(v1.Y * v2.Z - v1.Z * v2.Y,
			v1.Z * v2.X - v1.X * v2.Z, 
			v1.X * v2.Y - v1.Y * v2.X);
	}
	
	public static double GetTripleProduct(Vector v1, Vector v2, Vector v3)
	{
		return GetScalarProduct(v1, GetProduct(v2, v3));
	}
	
	public static double GetAngle(Vector v1, Vector v2)
	{
		double scalarProduct = GetScalarProduct(v1, v2);
		double v1Length = v1.GetLength();
		double v2Length = v2.GetLength();
		
		if (v1Length * v2Length == 0)
		{
			throw new ArgumentException("Divide by zero occurred");
		}
		
		return Math.Acos(scalarProduct / (v1Length * v2Length));
	}
	
	// Instance methods
	public double GetLength()
	{
		return Math.Sqrt(X * X + Y * Y + Z * Z);
	}
	
	public override bool Equals(object obj)
	{
		Vector resultVector = obj as Vector;
		
		if (resultVector == null)
		{
			return false;
		}
		
		return X == resultVector.X 
			&& Y == resultVector.Y 
			&& Z == resultVector.Z; 
	}
	
	public override int GetHashCode()
	{
		throw new NotSupportedException();
	}
	
	public override string ToString()
	{
		return string.Format("X, Y, Z = ({0}, {1}, {2})", X, Y, Z);
	}
}

public class Program
{
	public static void Main()
	{
		Console.WriteLine("{0}***VECTOR PROGRAM***{0}", Environment.NewLine);

        // Create two new vectors
        Console.WriteLine("---Create two vectors: firstVector, secondVector---");
        Vector firstVector = new Vector(1, 8, 3);
        Vector secondVector = new Vector(-4, 5, 7);

        Console.WriteLine("firstVector: {0}", firstVector);
        Console.WriteLine("secondVector: {0}{1}", secondVector, Environment.NewLine);

        // Add two vectors and save result in the third vector
        Console.WriteLine("---Add firstVector and secondVector---");
        Vector thirdVector = firstVector + secondVector; // rename or remove
        Console.WriteLine("firstVector + secondVector = thirdVector: {0}{1}", thirdVector, Environment.NewLine);
		
        // Subtract two vectors and save result in the fourth vector
        Console.WriteLine("---Subtract firstVector and secondVector---");
        Vector fourthVector = firstVector - secondVector;
        Console.WriteLine("firstVector - secondVector = fourthVector: {0}{1}", fourthVector, Environment.NewLine);

        // Get Scalar Product
        Console.WriteLine("---Scalar product from firstVector and secondVector---");
        Console.WriteLine("Scalar product: {0}{1}", Vector.GetScalarProduct(firstVector, secondVector), Environment.NewLine);

        // Get Vector Product
        Console.WriteLine("---Vector product from firstVector and secondVector---");
        Console.WriteLine("Vector product: {0}{1}", Vector.GetProduct(firstVector, secondVector), Environment.NewLine);

        // Get Triple Product
        Console.WriteLine("---Triple product from firstVector, secondVector, tripleVector---");
		Vector tripleVector = new Vector(2, 5, -2);
		Console.WriteLine("Create a new tripleVector: {0}{1}", tripleVector, Environment.NewLine);
        Console.WriteLine("Triple Product = {0}{1}",Vector.GetTripleProduct(firstVector, secondVector, tripleVector), Environment.NewLine);

        // Get Length of Vector
        Console.WriteLine("---Get length from firstVector---");
        Console.WriteLine("firstVector Length: {0}{1}", firstVector.GetLength(), Environment.NewLine);

        // Get Angle
        Console.WriteLine("---Angle between firstVector and secondVector---");
        Console.WriteLine("Angle between firstVector and secondVector: {0}{1}",
			Vector.GetAngle(firstVector, secondVector), Environment.NewLine);

        // Compare two vectors
        Console.WriteLine("---Compare firstVector with secondVector---");
        Console.WriteLine("Are firstVector and secondVector equals: {0}{1}", firstVector.Equals(secondVector), Environment.NewLine);

        Console.WriteLine("---Create fifthVector and compare firstVector and fifthVector---");
        Vector fifthVector = new Vector(1, 8, 3);
        Console.WriteLine("fifthVector: {0}", fifthVector);
        Console.WriteLine("Are firstVector and fifthVector equals: {0}", firstVector.Equals(fifthVector));
	}
}