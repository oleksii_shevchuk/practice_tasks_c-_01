using System;

public static class MatrixPrinter
{
    public static void Print(Matrix matrix)
    {
        for (int i = 0; i < matrix.RowCount; i++)
        {
            for (int j = 0; j < matrix.ColumnCount; j++)
            {
                Console.Write("\t{0}\t", matrix[i, j]);
            }
            Console.WriteLine();
        }
    }
}

public class Matrix
{
	// Private fields
	private int[,] _matrix;
	
	// Readonly fields
	public readonly int RowCount;
	public readonly int ColumnCount;
	
	// Constructors
	public Matrix(int rowCount, int columnCount)
	{
		RowCount = rowCount;
		ColumnCount = columnCount;
		_matrix = new int[rowCount, columnCount];
	}
	
	// Indexers
	public int this[int i, int j]
	{
		get 
		{
			if (i >= RowCount || i < 0 ||
				j >= ColumnCount || j < 0)
            {
                throw new IndexOutOfRangeException("Matrix index out of range method get");
            }
			
            return _matrix[i, j];
		}
		set
		{
			if (i >= RowCount || i < 0 ||
				j >= ColumnCount || j < 0)
            {
                throw new IndexOutOfRangeException("Matrix index out of range method get");
            }
			
			_matrix[i,j] = value;	
		}
	}
		
	// Operators
	public static Matrix operator + (Matrix matrix1, Matrix matrix2)
	{
		if (matrix1.RowCount != matrix2.RowCount || 
			matrix1.ColumnCount != matrix2.ColumnCount)
        {
            throw new ArgumentException("Size of two matrix not match");
        }
		
		Matrix resultMatrix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);

        for (int i = 0; i < resultMatrix.RowCount; i++)
        {
            for (int j = 0; j < resultMatrix.ColumnCount; j++)
            {
                resultMatrix[i, j] = matrix1[i, j] + matrix2[i, j];
            }
        }
		
        return resultMatrix;
	}
	
	public static Matrix operator - (Matrix matrix1, Matrix matrix2)
    {
        if (matrix1.RowCount != matrix2.RowCount ||
			matrix1.ColumnCount != matrix2.ColumnCount)
        {
            throw new Exception("Size of two matrix not match");
        }

		Matrix resultMatrix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);
		
        for (int i = 0; i < resultMatrix.RowCount; i++)
        {
            for (int j = 0; j < resultMatrix.ColumnCount; j++)
            {
                resultMatrix[i, j] = matrix1[i, j] - matrix2[i, j];
            }
        }
		
        return resultMatrix;
    }

    public static Matrix operator * (Matrix matrix1, Matrix matrix2)
    {
        if (matrix1.ColumnCount != matrix2.RowCount)
        {
            throw new Exception("Column Count from first matrix must matches Row Count of second matrix");
        }
		
        Matrix resultMatrix = new Matrix(Math.Min(matrix1.RowCount, matrix2.RowCount),
			Math.Min(matrix1.ColumnCount, matrix2.ColumnCount));

        for (int i = 0; i < resultMatrix.RowCount; i++)
        {
            for (int j = 0; j < resultMatrix.ColumnCount; j++)
            {
                resultMatrix[i, j] = 0;
				
                for (int k = 0; k < resultMatrix.RowCount; k++)
                {
                    resultMatrix[i, j] += matrix1[i, k] * matrix2[k, j];
                }
            }
        }
		
        return resultMatrix;
    }

    public static Matrix operator * (Matrix matrix, int value)
    {
        Matrix resultMatrix = new Matrix(matrix.RowCount, matrix.ColumnCount);
		
        for (int i = 0; i < matrix.RowCount; i++)
        {
            for (int j = 0; j < matrix.ColumnCount; j++)
            {
                resultMatrix[i, j] = matrix[i, j] * value;
            }
        }
		
        return resultMatrix;
    }
	
	public static Matrix operator / (Matrix matrix, int value)
    {
		if (value == 0)
		{
			throw new ArgumentException("Divide by zero occurred");
		}
		
        Matrix resultMatrix = new Matrix(matrix.RowCount, matrix.ColumnCount);
		
        for (int i = 0; i < matrix.RowCount; i++)
        {
            for (int j = 0; j < matrix.ColumnCount; j++)
            {
                resultMatrix[i, j] = matrix[i, j] / value;
            }
        }
		
        return resultMatrix;
    }
	
	// Instance methods
	public Matrix GetTransposedMatrix()
    {
        Matrix transposedMatrix = new Matrix(ColumnCount, RowCount);
		
        for (int i = 0; i < RowCount; i++)
        {
            for (int j = 0; j < ColumnCount; j++)
            {
                transposedMatrix[j, i] = _matrix[i, j];
            }
        }
		
        return transposedMatrix;
    }

    public Matrix GetSubmatrix(int rowCount, int columnCount)
    {
        if (rowCount > RowCount || columnCount > ColumnCount)
        {
            throw new ArgumentException("Submatrix index greater than initial matrix.");
        }
		
        Matrix submatrix = new Matrix(rowCount, columnCount);
		
        for (int i = 0; i < submatrix.RowCount; i++)
        {
            for (int j = 0; j < submatrix.ColumnCount; j++)
            {
                submatrix[i, j] = _matrix[i, j];
            }
        }
		
        return submatrix;
    }

    public override bool Equals(object obj)
    {
        Matrix comparableMatrix = obj as Matrix;
		
        if (comparableMatrix.RowCount != RowCount || comparableMatrix.ColumnCount != ColumnCount)
        {
            return false;
        }

        for (int i = 0; i < comparableMatrix.RowCount; i++)
        {
            for (int j = 0; j < comparableMatrix.ColumnCount; j++)
            {
                if (comparableMatrix[i, j] != _matrix[i, j])
                {
                    return false;
                }
            }
        }
        return true;
    }

    public override int GetHashCode()
    {
        throw new NotSupportedException();
    }
}

public class Program
{
	public static void Main()
	{
		Console.WriteLine("{0}***MATRIX PROGRAM***{0}", Environment.NewLine);
		
		try
        {
            Console.WriteLine("---Create new two matrix: firstMatrix and secondMatrix---{0}", Environment.NewLine);
            Matrix firstMatrix = new Matrix(2, 3);
            Matrix secondMatrix = new Matrix(3, 2);

            Random matrixRandom = new Random();
			
            // Assign values of firstMatrix and secondMatrix
            for (int i = 0; i < firstMatrix.RowCount; i++)
            {
                for (int j = 0; j < firstMatrix.ColumnCount; j++)
                {
                    firstMatrix[i, j] = matrixRandom.Next(0, 7);
                }
            }
			
            for (int i = 0; i < secondMatrix.RowCount; i++)
            {
                for (int j = 0; j < secondMatrix.ColumnCount; j++)
                {
                    secondMatrix[i, j] = matrixRandom.Next(0, 7);
                }
            }

            // Print two matrix
            Console.WriteLine("firstMatrix [{0},{1}]: ", firstMatrix.RowCount, firstMatrix.ColumnCount);
            MatrixPrinter.Print(firstMatrix);
            Console.WriteLine("secondMatrix: [{0},{1}]: ", secondMatrix.RowCount, secondMatrix.ColumnCount);
            MatrixPrinter.Print(secondMatrix);

            // Add firstMatrix and secondMatrix
            Console.WriteLine("---Create new thirdMatrix and fourthMatrix---{0}", Environment.NewLine);

            Matrix thirdMatrix = new Matrix(3, 3);
            Matrix fourthMatrix = new Matrix(3, 3);

            // Assign values of thirdMatrix and fourthMatrix
            for (int i = 0; i < thirdMatrix.RowCount; i++)
            {
                for (int j = 0; j < thirdMatrix.ColumnCount; j++)
                {
                    thirdMatrix[i, j] = j;
                }
            }
			
            for (int i = 0; i < fourthMatrix.RowCount; i++)
            {
                for (int j = 0; j < fourthMatrix.ColumnCount; j++)
                {
                    fourthMatrix[i, j] = j;
                }
            }

            Console.WriteLine("thirdMatrix: [{0},{1}]: ", thirdMatrix.RowCount, thirdMatrix.ColumnCount);
            MatrixPrinter.Print(thirdMatrix);
            Console.WriteLine("fourthMatrix: [{0},{1}]: ", fourthMatrix.RowCount, fourthMatrix.ColumnCount);
            MatrixPrinter.Print(fourthMatrix);

            Console.WriteLine("thirdMatrix + fourthMatrix: [{0},{1}]: ", (thirdMatrix + fourthMatrix).RowCount,
				(thirdMatrix + fourthMatrix).ColumnCount);
				
            MatrixPrinter.Print(thirdMatrix + fourthMatrix);

            // Multiplying firstMatrix and secondMatrix
            Console.WriteLine("firstMatrix * secondMatrix: [{0},{1}]: ",
				(firstMatrix * secondMatrix).RowCount, (firstMatrix * secondMatrix).ColumnCount);
				
            MatrixPrinter.Print(firstMatrix * secondMatrix);


            // Multiplying firstMatrix and valueForMultiplying
            int valueForMultiplying = 3; // multiplier
            Console.WriteLine("firstMatrix * {0}: [{1},{2}]:{3}", valueForMultiplying, (firstMatrix * valueForMultiplying).RowCount, 
				(firstMatrix * valueForMultiplying).ColumnCount, Environment.NewLine);
			
            MatrixPrinter.Print(firstMatrix * valueForMultiplying);

            // Get transposed firstMatrix
            Console.WriteLine("---Transposed firstMatrix---{0}", Environment.NewLine);
            MatrixPrinter.Print(firstMatrix.GetTransposedMatrix());

            // Get submatrix from firstMatrix
            Console.WriteLine("---Submatrix from firstMatrix---{0}", Environment.NewLine);

            int rowsOfSubmatrix = 2;
            int columnsOfSubmatrix = 2;
            Console.WriteLine("Get Submatrix [{0},{1}] from firstMatrix", rowsOfSubmatrix, columnsOfSubmatrix);
            MatrixPrinter.Print(firstMatrix.GetSubmatrix(2, 2));

            // Compare two matrix
            Console.WriteLine("---Compare thirdMatrix and fourthMatrix---{0}", Environment.NewLine);
            Console.WriteLine("Are thirdMatrix equals fourthMatrix: {0}", thirdMatrix.Equals(fourthMatrix));
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
	}	
}